**********************
Squares and Rectangles
**********************

|Rectangle Tool's icon| :kbd:`F4` or :kbd:`R`

To draw a rectangle:

#. Select the Rectangle tool from the toolbox on the left.
#. Click and drag the mouse diagonally, using the same motion as when dragging a
   selection box.

The rectangle will appear immediately after you release
the mouse button. To draw a perfect square, hold down :kbd:`Ctrl` while
click-dragging with the mouse.

The square-shaped handles can be used to change the rectangle's size.
However, when you only want to change either the height or the width of
the rectangle, it's hard to control with the mouse alone. Here you can
use :kbd:`Ctrl` to restrict the size change to either width or height
of the rectangle.

|image1|

*When you hold down Ctrl while dragging the square-shaped handles, it is
easy to limit the change in the rectangle's size to a single direction.*

The circle-shaped handles are for rounding the corners. Grab a circle
handle and move it just a tiny bit. You'll see that all four corners
will be rounded. Additionally, a second circular handle has appeared now. Each
handle changes the amount of rounding in one direction. It is not
possible to only round a single corner of the rectangle independant from
the others. They will all change together.

|The rectangle's circular handles|

*Moving the circle handles rounds the corners. Each circle handle
changes the radii in a different direction.*

|The rectangle's corner radii|

*The "Rx" and "Ry" values on the control bar, determine the radius of
the imaginary circle which the rounding is based upon.*

To restore the initial, sharp-cornered shape of a rectangle or square,
click on the far right icon in the tool control bar |Rectangle Tool: make corners
sharp icon|. This is very useful, when you're still learning how to master the
usage of the circular handles!

When you need to draw a rectangle with accurate dimensions, you can use
the tool controls bar:

- the field labelled :guilabel:`W` is for the width;
- the field labelled :guilabel:`H` is for the height;
- the :guilabel:`Rx` and :guilabel:`Ry` fields define the rounding radius for
  the corners.

The dropdown behind each number entry field allows you to select the unit you
need for your rectangle.

|Rectangle Tool's tool controls bar|

.. |Rectangle Tool's icon| image:: images/rectangle_tool.png
   :width: 50px
   :height: 50px
.. |image1| image:: images/rectangle_square_handles_1.png
.. |The rectangle's circular handles| image:: images/rectangle_circular_handles.png
.. |The rectangle's corner radii| image:: images/rectangle_corner_radii.png
.. |Rectangle Tool: make corners sharp icon| image:: images/rectangle_make_corners_sharp_icon.png
   :width: 50px
   :height: 50px
.. |Rectangle Tool's tool controls bar| image:: images/rect-tool-controls-bar_win10.png
