*******************************
Copying and Duplicating Objects
*******************************

The famous :guilabel:`Copy` keyboard shortcut :kbd:`Ctrl` + :kbd:`C` also works
in Inkscape, but its brother :guilabel:`Duplicate`, with :kbd:`Ctrl` + :kbd:`D`
is very useful, too.

Copying with :kbd:`Ctrl` + :kbd:`C` copies the selected element into the
clipboard. It can then be pasted into the document (or into another open
document) by using the corresponding shortcut :kbd:`Ctrl` + :kbd:`V`. This
inserts the object at the place of the cursor.

Duplicating, however, makes a copy of an object and positions this copy exactly
on top of the original. This can be very useful when your copy needs to be in
the same place as the original—you will not need to use the mouse or the
alignment functions to move it back.
